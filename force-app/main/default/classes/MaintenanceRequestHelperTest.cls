@isTest
public class MaintenanceRequestHelperTest {
    @IsTest
    static void ScheduleFutureRoutineCheckupTest() {
        Vehicle__c vehicle = new Vehicle__c(
            Model__c = 'Pop-Up Camper',
            Name = 'Pop-Up Camper',
            Bathrooms__c = 1,
            Bedrooms__c = 1
        );
        insert vehicle;
        System.debug('Insert vehicle');

        Product2 equipment1 = new Product2(
            Cost__c = 100,
            Maintenance_Cycle__c = 50,
            Name = 'Equipment 2',
            ProductCode = 'Breaker 13C',
            Replacement_Part__c = true
        );
        insert equipment1;
        System.debug('Insert equipment1');

        Product2 equipment2 = new Product2(
            Cost__c = 100,
            Maintenance_Cycle__c = 10,
            Name = 'Equipment 2',
            ProductCode = 'Generator 1000 kW',
            Replacement_Part__c = true
        );
        insert equipment2;
        System.debug('Insert equipment2');

        List<Case> repairsMaintenances = new List<Case>();
        for(Integer i = 0; i < 100; i++) {
            Case repairsMaintenance = new Case(
                Vehicle__c = vehicle.Id,
                Type = 'Repair',
                Status = 'New',
                Date_Reported__c = Date.today(),
                Date_Due__c = Date.today(),
    		    Subject = 'Teste' + i
            );  
            repairsMaintenances.add(repairsMaintenance);
        }
        insert repairsMaintenances;
        System.debug('Insert 100 repairsMaintenances');

        List<Case> routineMaintenances = new List<Case>();
        for(Integer i = 0; i < 100; i++) {
            Case routineMaintenance = new Case(
                Vehicle__c = vehicle.Id,
                Type = 'Routine Maintenance',
                Status = 'New',
                Date_Reported__c = Date.today(),
                Date_Due__c = Date.today()
            );  
            routineMaintenances.add(routineMaintenance);
        }
        insert routineMaintenances;
        System.debug('Insert 100 routineMaintenances');

        List<Case> otherMaintenances = new List<Case>();
        for(Integer i = 0; i < 100; i++) {
            Case otherMaintenance = new Case(
                Vehicle__c = vehicle.Id,
                Type = 'Other',
                Status = 'New',
                Date_Reported__c = Date.today(),
                Date_Due__c = Date.today(),
    		    Subject = 'Teste' + i
            );  
            otherMaintenances.add(otherMaintenance);
        }
        insert otherMaintenances;
        System.debug('Insert 100 otherMaintenances');

        Case maintenanceToTest = routineMaintenances.get(0);

        List<Equipment_Maintenance_Item__c> equipmentItens = new List<Equipment_Maintenance_Item__c>();

        Equipment_Maintenance_Item__c equipmentItem1 = new Equipment_Maintenance_Item__c(
            Equipment__c = equipment1.Id,
            Maintenance_Request__c = maintenanceToTest.Id,
            Quantity__c = 2
        );

        Equipment_Maintenance_Item__c equipmentItem2 = new Equipment_Maintenance_Item__c(
            Equipment__c = equipment2.Id,
            Maintenance_Request__c = maintenanceToTest.Id,
            Quantity__c = 5
        );

        equipmentItens.add(equipmentItem1);
        equipmentItens.add(equipmentItem2);
        insert equipmentItens;
        System.debug('Insert 2 equipmentItens');

        Test.startTest();
        List<Case> maintenanceToUpdate = new List<Case>();
        
        for(Case item : repairsMaintenances) {
            maintenanceToUpdate.add(new Case(
                Id = item.Id,
                Status = 'Closed'
            ));
        }

        for(Case item : routineMaintenances) {
            maintenanceToUpdate.add(new Case(
                Id = item.Id,
                Status = 'Closed'
            ));
        }

        for(Case item : otherMaintenances) {
            maintenanceToUpdate.add(new Case(
                Id = item.Id,
                Status = 'Closed'
            ));
        }

        update maintenanceToUpdate;
        System.debug('Update 300 maintenances requests');
        Test.stopTest();

        List<Case> allMaintenances = [SELECT Id FROM Case];
        System.assertEquals(500, allMaintenances.size());
    }
}
