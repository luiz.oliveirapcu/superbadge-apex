public with sharing class WarehouseCalloutService implements Queueable {
    private static String url = 'https://th-superbadge-apex.herokuapp.com/equipment?_ga=2.50744425.1988135348.1616414030-809229904.1586209617';

    public void execute(QueueableContext context) {
        runWarehouseEquipmentSync();
    }

    @future(callout=true) 
    public static void runWarehouseEquipmentSync() {
        HttpResponse res = calloutWarehouseEquipment();
        String bodyResult = res.getBody();

        List<WarehouseEquipment> warehouseEquipments = (List<WarehouseEquipment>) JSON.deserialize(
            bodyResult,
            List<WarehouseEquipment>.class
        );

        List<Product2> equipmentsToUpsert = new List<Product2>();
        if(warehouseEquipments != null && warehouseEquipments.size() > 0) {
            for(WarehouseEquipment warehouseEquipment : warehouseEquipments) {
                Product2 equipment = new Product2(
                    Replacement_Part__c = true,
                    Cost__c = warehouseEquipment.cost,
                    Current_Inventory__c = warehouseEquipment.quantity,
                    Lifespan_Months__c = warehouseEquipment.lifespan,
                    Maintenance_Cycle__c = warehouseEquipment.maintenanceperiod,
                    Warehouse_SKU__c = warehouseEquipment.sku,
                    Name = warehouseEquipment.name
                );

                equipmentsToUpsert.add(equipment);
            }

            upsert equipmentsToUpsert Warehouse_SKU__c;
        }
    }
    
    public static HttpResponse calloutWarehouseEquipment()
    {
        HttpResponse response = new Http().send(generateRequest());
        return response;
    }

    private static HttpRequest generateRequest() {
        HttpRequest req = new HttpRequest();
        req.setMethod('GET');
        req.setEndpoint(url);
        return req;
    }

    public class WarehouseEquipment {
        public Integer quantity {get;set;} 
        public String name {get;set;} 
        public Integer maintenanceperiod {get;set;} 
        public Integer lifespan {get;set;} 
        public Integer cost {get;set;} 
        public String sku {get;set;} 
    }
}


