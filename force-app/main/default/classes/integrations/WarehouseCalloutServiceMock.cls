@isTest
global class WarehouseCalloutServiceMock implements HttpCalloutMock{
    global HTTPResponse respond(HTTPRequest request) {
        System.assertEquals('GET', request.getMethod());

        StaticResource warehouseEquipmentsResource = [SELECT Id, Body FROM StaticResource WHERE Name = 'GetWarehouseEquipmentsResource' LIMIT 1];
        String warehouseEquipmentsBody = warehouseEquipmentsResource.Body.toString();

        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody(warehouseEquipmentsBody);
        response.setStatusCode(200);
        return response; 
    }
}
