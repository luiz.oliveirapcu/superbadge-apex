@isTest
private class WarehouseCalloutServiceTest {
    @isTest
    static void testWarehouseCalloutService() {
        StaticResource warehouseEquipmentsResource = [SELECT Id, Body FROM StaticResource WHERE Name = 'GetWarehouseEquipmentsResource' LIMIT 1];
        String warehouseEquipmentsBody = warehouseEquipmentsResource.Body.toString();

        Test.setMock(HttpCalloutMock.class, new WarehouseCalloutServiceMock()); 
        HttpResponse response = WarehouseCalloutService.calloutWarehouseEquipment();

        String contentType = response.getHeader('Content-Type');
        System.assert(contentType == 'application/json');

        String actualValue = response.getBody();
        System.debug(response.getBody());
        String expectedValue = warehouseEquipmentsBody;
        System.assertEquals(actualValue, expectedValue);

        System.assertEquals(200, response.getStatusCode());
    }

    @isTest
    static void testWarehouseCalloutServiceJobEnqueue() {
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new WarehouseCalloutServiceMock()); 
        System.enqueueJob(new WarehouseCalloutService());
        WarehouseCalloutService.runWarehouseEquipmentSync();
        Test.stopTest();

        System.assertEquals(22, [SELECT COUNT() FROM Product2]);
    }
}
