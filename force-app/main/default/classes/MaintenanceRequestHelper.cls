public with sharing class MaintenanceRequestHelper {
    public static void ScheduleFutureRoutineCheckup(List<Case> newMaintenanceRequests, Map<Id, Case> oldMaintenanceRequests)
    {
        List<Case> futureRoutinesCheckup = new List<Case>();
        Set<Id> closedMaintenancesIds = new Set<Id>();
        Set<Case> closedMaintenances = new Set<Case>();

        for(Case maintenanceRequest : newMaintenanceRequests) {
            if(maintenanceRequest.Status == 'Closed' && oldMaintenanceRequests.get(maintenanceRequest.Id).status != maintenanceRequest.status) {
                if(maintenanceRequest.Type == 'Repair' || maintenanceRequest.Type == 'Routine Maintenance') 
                {
                    closedMaintenances.add(maintenanceRequest);
                    closedMaintenancesIds.add(maintenanceRequest.Id);                    
                }
            }
        }

        if(closedMaintenances.size() == 0)
            return;

        Map<Id, List<Equipment_Maintenance_Item__c>> equipmentItensMap = getEquipmentItens(closedMaintenancesIds);
        
        for(Case closedMaintenance : closedMaintenances) {
            List<Equipment_Maintenance_Item__c> equipmentItens = equipmentItensMap.get(closedMaintenance.Id);
            Integer shortestEquipmentMaintCycle  = findShortestMaintCycle(equipmentItens);	

            Case futureRoutineCheckup = new Case(
                Vehicle__c = closedMaintenance.Vehicle__c,
                Type = 'Routine Maintenance',
                Status = 'New',
                Date_Reported__c = Date.today(),
                Date_Due__c = Date.today().addDays(shortestEquipmentMaintCycle),
    		    Subject = String.isBlank(closedMaintenance.Subject) ? 'Routine Maintenance Request' : closedMaintenance.Subject,
                ParentId = closedMaintenance.Id
            );           

            futureRoutinesCheckup.add(futureRoutineCheckup);
        }

        if(futureRoutinesCheckup.size() > 0)
            insert futureRoutinesCheckup;
    } 

    public static void SetEquipementsItensRoutineCheckup(List<Case> newMaintenanceRequests) {
        List<Equipment_Maintenance_Item__c> equipmentItensToCreate = new List<Equipment_Maintenance_Item__c>();
        Set<Id> parentsIds = new Set<Id>();
        Map<Id, Id> parentsIdchildIdsMap = new Map<Id, Id>();

        for(Case maintenanceRequest : newMaintenanceRequests) {
            if(maintenanceRequest.Status == 'New' && maintenanceRequest.Type == 'Routine Maintenance' && maintenanceRequest.ParentId != null) {
                parentsIds.add(maintenanceRequest.ParentId);
                parentsIdchildIdsMap.put(maintenanceRequest.ParentId, maintenanceRequest.Id);                    
            }
        }

        if(parentsIds.size() == 0)
            return;

        Map<Id, List<Equipment_Maintenance_Item__c>> equipmentItensMap = getEquipmentItens(parentsIds);

        for(Id parentId : parentsIdchildIdsMap.keySet()) {
            List<Equipment_Maintenance_Item__c> equipmentItens = equipmentItensMap.get(parentId);

            if(equipmentItens != null && equipmentItens.size() > 0)
            {
                for(Equipment_Maintenance_Item__c currentEquipmentItem : equipmentItens) {
                    Equipment_Maintenance_Item__c equipmentItem = new Equipment_Maintenance_Item__c(
                        Equipment__c = currentEquipmentItem.Equipment__c,
                        Maintenance_Request__c = parentsIdchildIdsMap.get(parentId),
                        Quantity__c = currentEquipmentItem.Quantity__c
                    );
                    equipmentItensToCreate.add(equipmentItem);
                }
            }
        }

        if(equipmentItensToCreate.size() > 0)
            insert equipmentItensToCreate;
    }
    
    private static Map<Id, List<Equipment_Maintenance_Item__c>> getEquipmentItens(Set<ID> maintenancesIds){
        Map<Id, List<Equipment_Maintenance_Item__c>>  equipmentItensMap = new Map<Id, List<Equipment_Maintenance_Item__c>>();

        List<Equipment_Maintenance_Item__c> equipmentsItens = [SELECT Id, Maintenance_Request__c, Quantity__c, Equipment__c, Equipment__r.Maintenance_Cycle__c 
                FROM Equipment_Maintenance_Item__c 
                WHERE Maintenance_Request__c IN : maintenancesIds
                ORDER BY Equipment__r.Maintenance_Cycle__c];

        for(Equipment_Maintenance_Item__c equipmentItem : equipmentsItens)
        {
            List<Equipment_Maintenance_Item__c> tempList;
    		if(equipmentItensMap.get(Id.valueOf(equipmentItem.Maintenance_Request__c)) == null) {
    			tempList = new List<Equipment_Maintenance_Item__c>();
    		}
            else {
    			tempList = equipmentItensMap.get(Id.valueOf(equipmentItem.Maintenance_Request__c));
    		}
    		tempList.add(equipmentItem);
    		equipmentItensMap.put(equipmentItem.Maintenance_Request__c, tempList);
        }

    	return equipmentItensMap;
    }

    private static Integer findShortestMaintCycle(List<Equipment_Maintenance_Item__c> equipmentItens){
    	Integer shortestValue = 0;

    	if(equipmentItens != null && equipmentItens.size() > 0) {
    		shortestValue = Integer.valueOf(equipmentItens.get(0).Equipment__r.Maintenance_Cycle__c);
    	}
    	
    	return shortestValue;
    }
}
