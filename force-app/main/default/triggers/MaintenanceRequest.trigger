trigger MaintenanceRequest on Case (after update, after insert) {
    if(Trigger.isAfter && Trigger.isUpdate)
        MaintenanceRequestHelper.ScheduleFutureRoutineCheckup(Trigger.new, Trigger.oldMap);

    if(Trigger.isAfter && Trigger.isInsert)
        MaintenanceRequestHelper.SetEquipementsItensRoutineCheckup(Trigger.new);
}